package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;
@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	private Set<Property> propertyCompareSet;
    public ComparePropertySessionBean() {
    	propertyCompareSet = new HashSet<>();
    }
	@Override
	public void addProperty(Property property) {
		propertyCompareSet.add(property);
		
	}

	@Override
	  public void removeProperty(Property property) {
        for (Property p : propertyCompareSet) {
        	if (p.getPropertyId() == property.getPropertyId()) {
        		propertyCompareSet.remove(p);
        		break;
        	}
        }
    }    

	@Override
	public int getBestPerRoom() {
		double bestPerRoom = 100000.00;
		int bestPropertyId = 0;
		for (Property p : propertyCompareSet) {
			if (p.getPrice()/p.getNumberOfBedrooms() <= bestPerRoom)
				bestPropertyId = p.getPropertyId();
		}
		return bestPropertyId;
	}

	@Override
	public CompareProperty create() throws CreateException, RemoteException {
		
		return null;
	}
	
}
