/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
	private ArrayList<Property> propertyList;
    public SimplePropertyRepositoryImpl(ArrayList<Property> propertyList) {
		super();
		this.propertyList = propertyList;
	}
	public SimplePropertyRepositoryImpl() {
        propertyList = new ArrayList<Property>();
    }
	public ArrayList<Property> getPropertyList() {
		return propertyList;
	}
	public void setPropertyList(ArrayList<Property> propertyList) {
		this.propertyList = propertyList;
	}
	@Override
	public void addProperty(Property property) throws Exception {
		propertyList.add(property);
	}
	@Override
	public Property searchPropertyById(int id) throws Exception {
		Property propertyResult = new Property();
		for(int index = 0; index < propertyList.size(); index++)
		{
			if(id == propertyList.get(index).getId())
			{
				propertyResult = propertyList.get(index);
			}
			
		}
		
		return propertyResult;
	}
	@Override
	public List<Property> getAllProperties() throws Exception {
		return propertyList;
	}
    
}
