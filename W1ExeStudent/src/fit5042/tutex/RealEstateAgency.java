package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.SimplePropertyRepositoryImpl;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty(){
    	int currentPropertyNumber;
    	int endPropertyNumber;
    	Property propertyOne = new Property(1,"24 Boston Ave, Malvern East VIC 3145, Australia",2, 150,420000);
    	Property propertyTwo = new Property(2,"11 Boston Ave, Carnegie VIC 3163, Australia", 2, 170,360000);
        Property propertyThree = new Property(3,"24 Anzac St, Malvern East VIC 3145, Australia", 4, 150,650000);
        Property propertyFour = new Property(4,"24 Macgregor Ave, Malvern East VIC 3145, Australia", 3, 140,435000);
        Property propertyFive = new Property(5,"1 Macgregor St, Malvern East VIC 3145, Australia", 2, 150,820000);
        try {
        	currentPropertyNumber = propertyRepository.getAllProperties().size();
			propertyRepository.addProperty(propertyOne);
			propertyRepository.addProperty(propertyTwo);
			propertyRepository.addProperty(propertyThree);
			propertyRepository.addProperty(propertyFour);
			propertyRepository.addProperty(propertyFive);
			endPropertyNumber = propertyRepository.getAllProperties().size();
			System.out.println(endPropertyNumber - currentPropertyNumber + " Properties added successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() throws Exception {
        for(int index = 0; index < propertyRepository.getAllProperties().size(); index++)
        {
        	System.out.println(propertyRepository.getAllProperties().get(index).getId() + " " + 
        propertyRepository.getAllProperties().get(index).getAddress() + " " + propertyRepository.getAllProperties().get(index).getNumberOfBedrooms()
         + "BR(s) " + propertyRepository.getAllProperties().get(index).getSize() + "sqm " + propertyRepository.getAllProperties().get(index).getPrice() + "$");
        }
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() throws Exception {
        System.out.print("Enter the ID of the property you want to search: ");
        Scanner scan = new Scanner(System.in);
        int searchId = scan.nextInt();
        Property resultProperty = new Property();
        resultProperty = propertyRepository.searchPropertyById(searchId);
        System.out.println(resultProperty.getId() + " " + resultProperty.getAddress() + " " + resultProperty.getNumberOfBedrooms() + " " +
        		resultProperty.getSize() + " " + resultProperty.getPrice());
    }
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
